---
vim: et ts=2 sts=2 sw=2
title: devenir internet: Atelier sobriété numérique niveau 0
keywords: [pédagogie, sobriété numérique, atelier]
author: Marc Chantreux <marc.chantreux@renater.fr>
licence: Attribution-ShareAlike 4.0 International
---

# Audience et cadre

L'objectif est de permettre aux apprenants:

* de comprendre le rôle des différents équipements impliqués dans internet
* d'avoir une intuition du problème de complexité algorithmique

pour permettre une relative autonomie dans:

* l'évaluation de la nocivité d'une pratique numérique
* la compréhension des recommendations proposées
* une auto-évaluation de leurs propres pratiques

l'idée est aussi de sensibiliser aux changement radicaux
nécessaires pour que le numérique puisse répondre aux
contraintes environementales.

# Moyens

* 2 intervenants
* des chemises, du papier
* des feutres
* des tables

# Rappels

* énergie grise
  * garder son terminal le plus longtemps possible
  * pas de serveur = pas de polution
  * pas de consommation active = mieux

# Trame


Principe:

Par l'introduction de besoins successifs, construire
un schema fonctionnel simplifié d'internet.

En même temps que les équipements apparaissent, affecter
des taches correspondantes aux participants pour mettre en
évidence la complexité algorithmique:

* le routeur a une table de routage sur une feuille A4
* le(s) serveur(s) DNS a une feuille A4 avec des correspondances
  domaines/@IP
* le serveur http a des chemises (répertoires) avec des
  feuilles: il en copie le contenu sur une feuille (la réponse)
  qui transite jusqu'au client
* le serveur applicatif doit prendre les instructions
  sur la feuille (inclure le contenu d'autres feuilles...)
  et rendre le résultat au serveur http (les CMS ...)
* le moteur de recherche doit chercher les données dans un corpus
  pour trouver l'url qui y est rattaché.
* parallèliser en distribuant les feuilles
  (patienter, c'est aimer la nature)
* le serveur de pub cherche la pub correspondant a la recherche
  (arrosoir -> pub bricomolo)
* le serveur de messagerie envoie un simple texte
* serveur anti-spam relit tout pour chercher les mots interdits
* le webmail doit copier une feuille qu'il cherche aupres du serveur
  de messagerie, la mettre dans une enveloppe "html" et le faire
  parvenir au serveur http qui envoie le message

Préambule:

prévenir que c'est très simplifié (pas de NAT, pas de DHCP,...):
l'idée est de comprendre, pas d'administrer.

### Jeu 0: un même écran mais pas la meme conso

consommation electrique sur le terminal
  * ouvrir le moniteur systeme (ou dispositif de mesure electrique?)
  * lire un texte simple (editeur de texte)
  * lire un texte formaté (libreoffice)
  * lire une page html avec le meme texte simple dans une video.
  * lancer un jeu video ???

rappeller que ce sont les ordres de grandeur qu'il faut avoir en tete
(* le nombre de document * le nombre d'utilisateurs)

## Jeu 1: social pollution network: ensemble, pourrissons la biosphère

* afficher 1 client, une imprimante et un switch.
  * même réseau physique
  * un nom (/etc/hosts)
  * 2 services (admin+impression)
    = 2 programmes
    = 2 ports
* comment faire pour connecter 2 switches distants?
  * afficher 2 routeurs: netid/hostid et table de routage
    (avoir un A4 avec un fichier de config d'un routeur
     avec des tables de routage statiques)
* une URL = un nom de machine + un chemin (chemise+feuille)
* trouver des besoins fonctionnels pour la recherche, la mise en forme
  et ainsi de suite ...

a la fin on comprend que:

* tout le monde a été actif
* on a pigé que
  *plus c'est gros moins ca passe
  * certaines taches sont bien plus chronophages que d'autres
* on aurait pu faire mieux (bookmark vs moteur de recherche ...)
* on a construit une URL
* on pige que refaire la meme manip c'est remobiliser tout le monde
  (serveur de stream ...)

### Jeu 2: se reconfigurer

* pour réduire les intermédiaires et les parcours
  * bookmark + historique + url vs moteur de recherche
  * blockers vs pub
  * télécharger mettre des proxies en place
  * virer le webmail
  * virer les fsync cloud
  * (TODO: ajouter ici ceux qui vous viennent)
* pour réduire les traitements: texte over tout le reste
* réduire les synchros. mieux: etre offline
  (rappeller que le wifi du tel bouffe)
  (rappeller que présence téléphonie != présence réseau)


### Jeu 3: micro-debat est-ce suffisant ?

* rappeller great acceleration et la pente à 5 degrés
* rappeller les proportions d'usages
* parler d'écologie radicale

  * le retour au tty
  * la fin de l'industrie du jeu video ?
  * la réorganisation de la diffusion multimédia
    (licence globale, projection publique, ...)
  * la fin de l'informatique personnelle

# Suites

Ateliers retour au tty (qui au passage s'approche de l'atelier eco-concetion
web statique en cours de préparation)


