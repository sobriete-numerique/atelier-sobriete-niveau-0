---
vim: et ts=2 sts=2 sw=2
title: Xwrite an article
keywords: X article literature communication
author: Marc Chantreux <marc.chantreux@renater.fr>
---

* préferer les couleurs sombres (mesures)

# faire savoir

* plus nous serons nombreux à pratiquer un numérique
  sobre et raisonné, plus nous permettons de futurs
  souhaitables.

* nos nouvelles habitudes numériques n'ont de sens que
  si ces pratiques sont partagées.


* la modification de notre usage du numérique n'a de sens
  que s


* les modifications de nos usages n'ont de sens que si
  nous 


* interpeler



# éteindre

* la box la nuit? le téléphone en mode avion?  comment être joignable?

* les périphériques apres usage (NAS, imprimante)
* multiprise pour mettre toute l'installation offline
* les interfaces réseau
  * wifi, bluetooth quand filaire
  * filaire quand pas besoin de net
  * adieu l'IM! (synchro mail à 45mn) (sauf rendez-vous/reunion)

# préferer

* les applications offline et les clients lourds (et non-electron)

# web

## consommer

* éviter les réseaux sociaux (rss!) (comment share? netpipe reborn?)
* éviter le webmail
* utiliser un blocker

## produire

* servir *statiquement*
* minifier (pug/mandoc)
* réduire les parcours, les widgets redondants
* version texte/gopher
* sources vs rendus
* etags

## consommer

  * moins
  * basse résolution
  * pas dans le navigateur (vlc)
    (vlc = 50% de cpu en moins par rapport au player web (stat perso, conso))

## accéder

  * preferer
    * la projection publique
    * la médiathèque
    * le partage de copies privées
       (oui c'est illégal ... est-ce vraiment encore un argument?)
  * télécharger plutôt que streamer (sauf one shot/test)

## produire

  * le podcast (+ synchro html/svg?)
  * plans statiques
  * résolutions max adaptée

# documentation

* dans l'ordre
  * copie locale
  * historique et bookmarks
  * liens vers ...
  * moteur de recherche local
  * moteur de recherche général
  * webring
* fermer les onglets inutiles (onetab)

# échanger

* via clef/bluetooth vs tcp
* dans l'ordre (verification, mesure)
  * clef physique
  * wifi ad-hocs et bluetooth
  * p2p ???
  * download
  * stream
* jeux via bluetooth vs tcp ???
* data over TNT?

# besoin d'alternatives

* etherpad???
